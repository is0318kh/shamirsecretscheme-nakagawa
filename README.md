README FILE
Masato Nakagawa

Mac mosquitto broker起動
port 1883の時
$ /usr/local/opt/mosquitto/sbin/mosquitto

port 8883の時
$ /usr/local/opt/mosquitto/sbin/mosquitto -c /usr/local/opt/mosquitto/etc/mosquitto/mosquitto.conf


Brokerが起動しない時のエラー処理
$ ps -ef | grep mosquitto
$ sudo kill ○○
○にはmosquittoが使用しているポートをいれる

コンパイル方法
Publisherの場合
gcc -lgmp -o main main.c -lmosquitto
./main -s k n
(k,nには数字をいれる. k<=nにすること)

Subscriberの場合
gcc -lgmp -o main main.c -lmosquitto
./main -c k
(kには数字をいれる. Publisherで用いたkと同一のもの)

RSAの場合
gcc -o sum -lgmp rsa.c mosquitto.c -lmosquitto
./sum plain.txt
plain.txtには事前に共通鍵を入れておく

Raspberry pi を起動

ターミナルバージョンの確認
$ cat /etc/debian_version

設定ファイルの操作
$ sudo raspi-config
5 Interfacing Optionsからsshを有効化

固定IPアドレスの設定
$ sudo nano /etc/dhcpcd.conf
末端に
interface wlan0
static ip_address=192.168.1.X/24
を記入して保存
変更の確認
$ sudo cat /etc/dhcpcd.conf

固有IPアドレスを設定しないとSSH接続の時に毎回確認が必要になるため面倒

sshファイルの作成
$ cd /boot
$ sudo su
$ mkdir ssh
$ mkdir ssh.txt

wifiの設定
$ cd /etc
$ sudo nano wpa_supplicant/wpa_supplicant.conf
書き込み内容
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=JP
network={
ssid="CC-NSL"
psk="kawaimakoto"
}
変更の確認
$ sudo cat wpa_supplicant/wpa_supplicant.conf

DNSの設定
$ sudo nano /etc/resolv.conf
書き込み内容
nameserver 192.168.1.X
に書き換え保存
確認
$ sudo cat /etc/resolv.conf
再起動
$ sudo /etc/init.d/networking reload

mosquittoのダウンロード
https://mosquitto.org/download/
より最新のmosquittoをダウンロード
$ cd Downloads
$ mv mosquito-1.6.7.tar.gz /usr/sbin
$ cd /usr/sbin
$ tar -zxvf mosquitto.tar.gz

tar.gzの解凍
$ tar -zxvf mosquito.tar.gz

tar.xzの解凍
$ tar -Jxvf gmpa-6.1.2.tar.xz

ネットにつながらない時
$ sudo dhclient wlan0

Raspberry pi aptitude install手順
$ apt-get install aptitude
$ aptitude search mosquitto
$ apt-get install --fix-missing

Raspberry pi os確認手順
$ cat /etc/os-release

Raspberry pi openssl install手順
$ mkdir openssl
$ cd openssl
$ sudo wget https://www.openssl.org/source/openssl-1.1.1g.tar.gz
$ tar xf openssl-1.1.1g.tar.gz
$ cd openssl-1.1.1g
$ ./config zlib shared no-ssl3
$ make -j4
$ sudo make install

Raspberry pi mosquitto install手順
$ sudo wget http://repo.mosquitto.org/debian/mosquitto.gpg.key
$ sudo apt-key add mosquitto-repo.gpg.key
$ cd /etc/apt/sources.list.d/
$ sudo wget http://repo.mosquitto.org/debian/mosquitto-buster.list
or
$ sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list
$ sudo wget http://repo.mosquitto.org/debian/mosquitto-stretch.list
$ apt-get update
$ apt-cache search mosquitto
$ apt-get install mosquitto

pbc.h install手順
$ wget https://crypto.stanford.edu/pbc/files/pbc-0.5.14.tar.gz
$ tar zxvf pbc-0.5.14.tar.gz
$ ./configure
$ make
$ sudo make install

ssh接続手順
ssh pi@ip_address
password : raspberry
