#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <unistd.h>
#include <gmp.h>
#include <time.h>
#include <sys/time.h>

#include <signal.h>
#include <sys/stat.h>
#include <mosquitto.h>

struct mosquitto *mosq;

void endCatch(int sig) {
    mosquitto_destroy(mosq);
    mosquitto_lib_cleanup();
    printf("End Program\n");
    exit(0);
}

static void printHex(const char *title, const unsigned char *s, int len) {
    int n;

    printf("%s: ", title);
    for(n=0; n<len; ++n) {
        if((n%16) == 0) {
            printf("\n%04x", n);
        }
        printf(" %02x", s[n]);
    }
    printf("\n");
}

unsigned char* read_File(int *len, char *fname) {
    FILE *fp;
    struct stat sbuf;
    unsigned char *data;

    if(len == NULL || fname == NULL)
        return NULL;
    if(stat(fname, &sbuf) == -1)
        return NULL;

    *len = (int)sbuf.st_size;
    data = (unsigned char*)malloc(*len);

    if(!data)
        return NULL;
    if((fp = fopen(fname, "rb")) == NULL)
        return NULL;
    if(fread(data, *len, 1, fp) < 1) {
        fclose(fp);
        return NULL;
    }

    printHex("DATA", data, *len);

    fclose(fp);

    return data;
}

void mosquitto(){
  const char *ip = "192.168.3.7"; //自宅
  //const char *ip = "172.31.175.85"; //研究室
  const int port = 1883;
  const char *username = "topsecret";
  const char *password = "password";
  const char *id = "RASPBERRYPI";
  const char *topic = "both";
  const int sleepTime = 2;
  FILE *fp;

  struct timeval startTime, endTime;
  clock_t startClock, endClock;

  gettimeofday(&startTime, NULL);
  startClock = clock();

  signal(SIGINT, endCatch);

  printf("Prepareting mosquitto...\n");
  mosquitto_lib_init();
  mosq = mosquitto_new(id, 0, NULL);

  if(! mosq) {
      perror("Failed to create mosquitto.\n");
      mosquitto_lib_cleanup();
      //return EXIT_FAILURE;
  }

  unsigned char *intext;
  int inLen = 0;

  intext = read_File(&inLen, "enc.txt");

  mosquitto_username_pw_set(mosq, username, password);
  mosquitto_connect(mosq, ip, port, 60);
  printf("Server connected!\n");
  mosquitto_publish(mosq, NULL, topic, strlen(intext), intext, 0, 0);
  printf("Publish \"%s\" about \"%s\" from %s to %s : %d\n", intext, topic, id, ip, port);


  gettimeofday(&endTime, NULL);
  endClock = clock();

  time_t diffsec = difftime(endTime.tv_sec, startTime.tv_sec); // 差分処理
  suseconds_t diffsub = endTime.tv_usec-startTime.tv_usec;     // マイクロ秒部分の差分処理
  fprintf(stdout, "計測時間 : %ld.%.4ld\n", diffsec, diffsub);

  double cpusec = (double)(endClock-startClock)/(double)CLOCKS_PER_SEC;
  fprintf(stdout, "CPU時間 : %.4f\n", cpusec);


  fp = fopen("data2.txt", "a");
  fprintf(fp, "%ld.%.4ld\n", diffsec, diffsub);
  fclose(fp);


  mosquitto_destroy(mosq);
  mosquitto_lib_cleanup();
  printf("End Program\n");
  printf("\n");

}
