#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <unistd.h>
#include <gmp.h>
#include <time.h>
#include <sys/time.h>

#include <signal.h>
#include <sys/stat.h>
#include <mosquitto.h>

#define DELIMITER "."
#define BLOCK_ALLOCATION_SIZE 1024

typedef struct MPZNode_t {
    struct MPZNode_t * next;
    mpz_t x;
    mpz_t y;
} MPZNode;

typedef struct {
    mpz_t * x_vals;
    mpz_t * y_vals;
    size_t num_shares;
} Shares;

typedef struct {
    Shares * chunks;
    size_t num_chunks;
} ShareChunks;

mpz_t modulus;
size_t modulus_max_bytes;
gmp_randstate_t rstate;

struct mosquitto *mosq = NULL;

const char *topic = "both";
int mqtt_flag = true;
int count = 0;

unsigned char
addA[1024], addB[1024], addC[1024], addD[1024], addE[1024],
addF[1024], addG[1024], addH[1024], addI[1024], addJ[1024],
addK[1024], addL[1024], addM[1024], addN[1024], addO[1024],
addP[1024], addQ[1024], addR[1024], addS[1024], addT[1024],
addU[1024], addV[1024], addW[1024], addX[1024], addY[1024],
addZ[1024], addAA[1024], addAB[1024], addAC[1024], addAD[1024],
addAE[1024], addAF[1024], addAG[1024], addAH[1024], addAI[1024],
addAJ[1024], addAK[1024], addAL[1024], addAM[1024], addAN[1024],
addAO[1024], addAP[1024], addAQ[1024], addAR[1024], addAS[1024],
addAT[1024], addAU[1024], addAV[1024], addAW[1024], addAX[1024],
addAY[1024], addAZ[1024], addBA[1024], addBB[1024], addBC[1024],
addBD[1024], addBE[1024], addBF[1024], addBG[1024], addBH[1024],
addBI[1024], addBJ[1024], addBK[1024], addBL[1024], addBM[1024],
addBN[1024], addBO[1024], addBP[1024], addBQ[1024], addBR[1024],
addBS[1024], addBT[1024], addBU[1024], addBV[1024], addBW[1024],
addBX[1024], addBY[1024], addBZ[1024], addCA[1024], addCB[1024],
addCC[1024], addCD[1024], addCE[1024], addCF[1024], addCG[1024],
addCH[1024], addCI[1024], addCJ[1024], addCK[1024], addCL[1024],
addCM[1024], addCN[1024], addCO[1024], addCP[1024], addCQ[1024],
addCR[1024], addCS[1024], addCT[1024], addCU[1024], addCV[1024];

int
inLenA, inLenB, inLenC, inLenD, inLenE,
inLenF, inLenG, inLenH, inLenI, inLenJ,
inLenK, inLenL, inLenM, inLenN, inLenO,
inLenP, inLenQ, inLenR, inLenS, inLenT,
inLenU, inLenV, inLenW, inLenX, inLenY,
inLenZ, inLenAA, inLenAB, inLenAC, inLenAD,
inLenAE, inLenAF, inLenAG, inLenAH, inLenAI,
inLenAJ, inLenAK, inLenAL, inLenAM, inLenAN,
inLenAO, inLenAP, inLenAQ, inLenAR, inLenAS,
inLenAT, inLenAU, inLenAV, inLenAW, inLenAX,
inLenAY, inLenAZ, inLenBA, inLenBB, inLenBC,
inLenBD, inLenBE, inLenBF, inLenBG, inLenBH,
inLenBI, inLenBJ, inLenBK, inLenBL, inLenBM,
inLenBN, inLenBO, inLenBP, inLenBQ, inLenBR,
inLenBS, inLenBT, inLenBU, inLenBV, inLenBW,
inLenBX, inLenBY, inLenBZ, inLenCA, inLenCB,
inLenCC, inLenCD, inLenCE, inLenCF, inLenCG,
inLenCH, inLenCI, inLenCJ, inLenCK, inLenCL,
inLenCM, inLenCN, inLenCO, inLenCP, inLenCQ,
inLenCR, inLenCS, inLenCT, inLenCU, inLenCV;


//////////////////////////////////////////////////////////////////////
//                    shamir secret scheme                          //
//////////////////////////////////////////////////////////////////////

/*モジュールの初期化*/
void initialize_with_modulus(mpz_t global_modulus, size_t max_bytes) {
    mpz_t max_secret_value;
    mpz_init(max_secret_value);
    mpz_ui_pow_ui(max_secret_value, 2, max_bytes*8);
    assert(mpz_cmp(global_modulus, max_secret_value) >= 0);
    mpz_clear(max_secret_value);

    // モジュールの設定
    mpz_set(modulus, global_modulus);
    modulus_max_bytes = max_bytes;

    // ランダムな初期化
    unsigned long int rseed = random();
    gmp_randinit_default(rstate);
    gmp_randseed_ui(rstate, rseed);
}

/*グローバル変数の初期化*/
void initialize() {
    mpz_t tmp;
    mpz_init(tmp);
    mpz_ui_pow_ui(tmp, 2, 521);
    mpz_sub_ui(tmp, tmp, 1);
    initialize_with_modulus(tmp, 64);
    mpz_clear(tmp);
}

/*メモリの解放*/
void cleanup() {
    mpz_clear(modulus);
    gmp_randclear(rstate);
}

void free_shares(Shares * shares) {
    for(size_t idx=0; idx<shares->num_shares; idx++) {
        mpz_clear(shares->x_vals[idx]);
        mpz_clear(shares->y_vals[idx]);
    }

    free(shares->x_vals);
    free(shares->y_vals);
    shares->x_vals = NULL;
    shares->y_vals = NULL;
    shares->num_shares = 0;
}

void free_chunks(ShareChunks * chunks) {
    if(! chunks || ! chunks->chunks)
        return;

    for(size_t idx=0; idx<chunks->num_chunks; idx++)
        free_shares(chunks->chunks+idx);

    free(chunks->chunks);
    chunks->chunks = NULL;
    chunks->num_chunks = 0;
}

/**************************************************
 *                  chunkの表示
***************************************************/
void print_chunks(FILE * stream, const ShareChunks chunks) {
    // 空でないことの確認
    if(! chunks.num_chunks || ! chunks.chunks->num_shares)
        return;

    size_t num_chunks = chunks.num_chunks;                   // chunk数の取得
    size_t num_shares_per_chunk = chunks.chunks->num_shares; // chunkのシェア数を取得

    // 共有間の行方向ループ
    for(size_t i=0; i<num_shares_per_chunk; i++) {
        // 共有間の列方向ループ
        for(size_t j=0; j<num_chunks; j++) {
            Shares * shares = chunks.chunks+j;

            // 区切られたx, y値を出力
            mpz_out_str(stream, 16, shares->x_vals[i]);
            fprintf(stream, DELIMITER);
            mpz_out_str(stream, 16, shares->y_vals[i]);

            if(j < num_chunks-1)
                fprintf(stream, DELIMITER);
        }
        fprintf(stream, "\n");
    }
}

/*decode*/
void decode_message(char * message, const mpz_t encoding) {
    size_t count;
    mpz_export(message, &count, 1, 1, 0, 0, encoding);
    message[count] = '\0';
}

/*結合時のシェアの作成*/
void combine_shares(mpz_t secret, const Shares shares, const size_t k) {
    mpz_set_ui(secret, 0);

    mpz_t yj;
    mpz_t xm;
    mpz_t xm_xj;
    mpz_t buffer;
    mpz_init(yj);
    mpz_init(xm);
    mpz_init(xm_xj);
    mpz_init(buffer);

    for (size_t j=0; j<k; j++) {
        mpz_set(yj, shares.y_vals[j]);
        mpz_set_ui(buffer, 1);
        for (size_t m=0; m<k; m++) {
            if (m != j) {
                mpz_set(xm, shares.x_vals[m]);
                mpz_sub(xm_xj, xm, shares.x_vals[j]);
                mpz_invert(xm_xj, xm_xj, modulus);
                mpz_mul(xm, xm, xm_xj);
                mpz_mod(xm, xm, modulus);
                mpz_mul(buffer, buffer, xm);
                mpz_mod(buffer, buffer, modulus);
            }
        }
        mpz_mul(yj, yj, buffer);
        mpz_mod(yj, yj, modulus);
        mpz_add(secret, secret, yj);
        mpz_mod(secret, secret, modulus);
    }
    mpz_clear(yj);
    mpz_clear(xm);
    mpz_clear(xm_xj);
    mpz_clear(buffer);
}

/*結合*/
void combine_chunks(char * message, const ShareChunks chunks, const size_t k) {
    mpz_t secret;
    mpz_init(secret);

    for(size_t chunk_idx=0; chunk_idx<chunks.num_chunks; chunk_idx++) {
        size_t chunk_begin_offset = chunk_idx*modulus_max_bytes;
        Shares * shares = chunks.chunks+chunk_idx;
        combine_shares(secret, *shares, k);

        //Decode
        decode_message(message+chunk_begin_offset, secret);
    }
    mpz_clear(secret);
}

/*chunkの読み取り*/
//int read_chunks(const char *strA, const char *strB, const char *strC, ShareChunks * chunks, const size_t k) {
int read_chunks(const char *strA, const char *strB, const char *strC, const char *strD, const char *strE, ShareChunks * chunks, const size_t k) {
/*
int read_chunks(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  ShareChunks * chunks, const size_t k) {
*/
/*
int read_chunks(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  ShareChunks * chunks, const size_t k) {
*/
/*
int read_chunks(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  ShareChunks * chunks, const size_t k) {
*/
/*
int read_chunks(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  ShareChunks * chunks, const size_t k) {
*/
/*
int read_chunks(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  ShareChunks * chunks, const size_t k) {
*/
/*
int read_chunks(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  const char *strAY, const char *strAZ, const char *strBA, const char *strBB, const char *strBC,
  const char *strBD, const char *strBE, const char *strBF, const char *strBG, const char *strBH,
  ShareChunks * chunks, const size_t k) {
*/
/*
int read_chunks(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  const char *strAY, const char *strAZ, const char *strBA, const char *strBB, const char *strBC,
  const char *strBD, const char *strBE, const char *strBF, const char *strBG, const char *strBH,
  const char *strBI, const char *strBJ, const char *strBK, const char *strBL, const char *strBM,
  const char *strBN, const char *strBO, const char *strBP, const char *strBQ, const char *strBR,
  ShareChunks * chunks, const size_t k) {
*/
/*
int read_chunks(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  const char *strAY, const char *strAZ, const char *strBA, const char *strBB, const char *strBC,
  const char *strBD, const char *strBE, const char *strBF, const char *strBG, const char *strBH,
  const char *strBI, const char *strBJ, const char *strBK, const char *strBL, const char *strBM,
  const char *strBN, const char *strBO, const char *strBP, const char *strBQ, const char *strBR,
  const char *strBS, const char *strBT, const char *strBU, const char *strBV, const char *strBW,
  const char *strBX, const char *strBY, const char *strBZ, const char *strCA, const char *strCB,
  ShareChunks * chunks, const size_t k) {
*/
/*
int read_chunks(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  const char *strAY, const char *strAZ, const char *strBA, const char *strBB, const char *strBC,
  const char *strBD, const char *strBE, const char *strBF, const char *strBG, const char *strBH,
  const char *strBI, const char *strBJ, const char *strBK, const char *strBL, const char *strBM,
  const char *strBN, const char *strBO, const char *strBP, const char *strBQ, const char *strBR,
  const char *strBS, const char *strBT, const char *strBU, const char *strBV, const char *strBW,
  const char *strBX, const char *strBY, const char *strBZ, const char *strCA, const char *strCB,
  const char *strCC, const char *strCD, const char *strCE, const char *strCF, const char *strCG,
  const char *strCH, const char *strCI, const char *strCJ, const char *strCK, const char *strCL,
  ShareChunks * chunks, const size_t k) {
*/
/*
int read_chunks(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  const char *strAY, const char *strAZ, const char *strBA, const char *strBB, const char *strBC,
  const char *strBD, const char *strBE, const char *strBF, const char *strBG, const char *strBH,
  const char *strBI, const char *strBJ, const char *strBK, const char *strBL, const char *strBM,
  const char *strBN, const char *strBO, const char *strBP, const char *strBQ, const char *strBR,
  const char *strBS, const char *strBT, const char *strBU, const char *strBV, const char *strBW,
  const char *strBX, const char *strBY, const char *strBZ, const char *strCA, const char *strCB,
  const char *strCC, const char *strCD, const char *strCE, const char *strCF, const char *strCG,
  const char *strCH, const char *strCI, const char *strCJ, const char *strCK, const char *strCL,
  const char *strCM, const char *strCN, const char *strCO, const char *strCP, const char *strCQ,
  const char *strCR, const char *strCS, const char *strCT, const char *strCU, const char *strCV,
  ShareChunks * chunks, const size_t k) {
*/
    // getline()から読み取るブッファの作成
    char * line = malloc(BLOCK_ALLOCATION_SIZE);
    size_t line_allocation = BLOCK_ALLOCATION_SIZE;
    ssize_t line_length = 0;

    // 現在の行番号を保持する
    size_t line_idx = 0;

    char * save_state = NULL;
    mpz_t x;
    mpz_t y;
    mpz_init(x);
    mpz_init(y);

    while(line_idx < k && line_length != -1) {
        switch(line_idx) {
            case 0: strncpy(line, strA, line_allocation); line_length = strlen(line);
                    //printf("%s\nlength = %d\n\n", line, line_length);
                    break;
            case 1: strncpy(line, strB, line_allocation); line_length = strlen(line); break;
            case 2: strncpy(line, strC, line_allocation); line_length = strlen(line); break;
            case 3: strncpy(line, strD, line_allocation); line_length = strlen(line); break;
            case 4: strncpy(line, strE, line_allocation); line_length = strlen(line); break;
            /*
            case 5: strncpy(line, strF, line_allocation); line_length = strlen(line); break;
            case 6: strncpy(line, strG, line_allocation); line_length = strlen(line); break;
            case 7: strncpy(line, strH, line_allocation); line_length = strlen(line); break;
            case 8: strncpy(line, strI, line_allocation); line_length = strlen(line); break;
            case 9: strncpy(line, strJ, line_allocation); line_length = strlen(line); break;

            case 10: strncpy(line, strK, line_allocation); line_length = strlen(line); break;
            case 11: strncpy(line, strL, line_allocation); line_length = strlen(line); break;
            case 12: strncpy(line, strM, line_allocation); line_length = strlen(line); break;
            case 13: strncpy(line, strN, line_allocation); line_length = strlen(line); break;
            case 14: strncpy(line, strO, line_allocation); line_length = strlen(line); break;
            case 15: strncpy(line, strP, line_allocation); line_length = strlen(line); break;
            case 16: strncpy(line, strQ, line_allocation); line_length = strlen(line); break;
            case 17: strncpy(line, strR, line_allocation); line_length = strlen(line); break;
            case 18: strncpy(line, strS, line_allocation); line_length = strlen(line); break;
            case 19: strncpy(line, strT, line_allocation); line_length = strlen(line); break;

            case 20: strncpy(line, strU, line_allocation); line_length = strlen(line); break;
            case 21: strncpy(line, strV, line_allocation); line_length = strlen(line); break;
            case 22: strncpy(line, strW, line_allocation); line_length = strlen(line); break;
            case 23: strncpy(line, strX, line_allocation); line_length = strlen(line); break;
            case 24: strncpy(line, strY, line_allocation); line_length = strlen(line); break;
            case 25: strncpy(line, strZ, line_allocation); line_length = strlen(line); break;
            case 26: strncpy(line, strAA, line_allocation); line_length = strlen(line); break;
            case 27: strncpy(line, strAB, line_allocation); line_length = strlen(line); break;
            case 28: strncpy(line, strAC, line_allocation); line_length = strlen(line); break;
            case 29: strncpy(line, strAD, line_allocation); line_length = strlen(line); break;

            case 30: strncpy(line, strAE, line_allocation); line_length = strlen(line); break;
            case 31: strncpy(line, strAF, line_allocation); line_length = strlen(line); break;
            case 32: strncpy(line, strAG, line_allocation); line_length = strlen(line); break;
            case 33: strncpy(line, strAH, line_allocation); line_length = strlen(line); break;
            case 34: strncpy(line, strAI, line_allocation); line_length = strlen(line); break;
            case 35: strncpy(line, strAJ, line_allocation); line_length = strlen(line); break;
            case 36: strncpy(line, strAK, line_allocation); line_length = strlen(line); break;
            case 37: strncpy(line, strAL, line_allocation); line_length = strlen(line); break;
            case 38: strncpy(line, strAM, line_allocation); line_length = strlen(line); break;
            case 39: strncpy(line, strAN, line_allocation); line_length = strlen(line); break;

            case 40: strncpy(line, strAO, line_allocation); line_length = strlen(line); break;
            case 41: strncpy(line, strAP, line_allocation); line_length = strlen(line); break;
            case 42: strncpy(line, strAQ, line_allocation); line_length = strlen(line); break;
            case 43: strncpy(line, strAR, line_allocation); line_length = strlen(line); break;
            case 44: strncpy(line, strAS, line_allocation); line_length = strlen(line); break;
            case 45: strncpy(line, strAT, line_allocation); line_length = strlen(line); break;
            case 46: strncpy(line, strAU, line_allocation); line_length = strlen(line); break;
            case 47: strncpy(line, strAV, line_allocation); line_length = strlen(line); break;
            case 48: strncpy(line, strAW, line_allocation); line_length = strlen(line); break;
            case 49: strncpy(line, strAX, line_allocation); line_length = strlen(line); break;

            case 50: strncpy(line, strAY, line_allocation); line_length = strlen(line); break;
            case 51: strncpy(line, strAZ, line_allocation); line_length = strlen(line); break;
            case 52: strncpy(line, strBA, line_allocation); line_length = strlen(line); break;
            case 53: strncpy(line, strBB, line_allocation); line_length = strlen(line); break;
            case 54: strncpy(line, strBC, line_allocation); line_length = strlen(line); break;
            case 55: strncpy(line, strBD, line_allocation); line_length = strlen(line); break;
            case 56: strncpy(line, strBE, line_allocation); line_length = strlen(line); break;
            case 57: strncpy(line, strBF, line_allocation); line_length = strlen(line); break;
            case 58: strncpy(line, strBG, line_allocation); line_length = strlen(line); break;
            case 59: strncpy(line, strBH, line_allocation); line_length = strlen(line); break;

            case 60: strncpy(line, strBI, line_allocation); line_length = strlen(line); break;
            case 61: strncpy(line, strBJ, line_allocation); line_length = strlen(line); break;
            case 62: strncpy(line, strBK, line_allocation); line_length = strlen(line); break;
            case 63: strncpy(line, strBL, line_allocation); line_length = strlen(line); break;
            case 64: strncpy(line, strBM, line_allocation); line_length = strlen(line); break;
            case 65: strncpy(line, strBN, line_allocation); line_length = strlen(line); break;
            case 66: strncpy(line, strBO, line_allocation); line_length = strlen(line); break;
            case 67: strncpy(line, strBP, line_allocation); line_length = strlen(line); break;
            case 68: strncpy(line, strBQ, line_allocation); line_length = strlen(line); break;
            case 69: strncpy(line, strBR, line_allocation); line_length = strlen(line); break;

            case 70: strncpy(line, strBS, line_allocation); line_length = strlen(line); break;
            case 71: strncpy(line, strBT, line_allocation); line_length = strlen(line); break;
            case 72: strncpy(line, strBU, line_allocation); line_length = strlen(line); break;
            case 73: strncpy(line, strBV, line_allocation); line_length = strlen(line); break;
            case 74: strncpy(line, strBW, line_allocation); line_length = strlen(line); break;
            case 75: strncpy(line, strBX, line_allocation); line_length = strlen(line); break;
            case 76: strncpy(line, strBY, line_allocation); line_length = strlen(line); break;
            case 77: strncpy(line, strBZ, line_allocation); line_length = strlen(line); break;
            case 78: strncpy(line, strCA, line_allocation); line_length = strlen(line); break;
            case 79: strncpy(line, strCB, line_allocation); line_length = strlen(line); break;

            case 80: strncpy(line, strCC, line_allocation); line_length = strlen(line); break;
            case 81: strncpy(line, strCD, line_allocation); line_length = strlen(line); break;
            case 82: strncpy(line, strCE, line_allocation); line_length = strlen(line); break;
            case 83: strncpy(line, strCF, line_allocation); line_length = strlen(line); break;
            case 84: strncpy(line, strCG, line_allocation); line_length = strlen(line); break;
            case 85: strncpy(line, strCH, line_allocation); line_length = strlen(line); break;
            case 86: strncpy(line, strCI, line_allocation); line_length = strlen(line); break;
            case 87: strncpy(line, strCJ, line_allocation); line_length = strlen(line); break;
            case 88: strncpy(line, strCK, line_allocation); line_length = strlen(line); break;
            case 89: strncpy(line, strCL, line_allocation); line_length = strlen(line); break;

            case 90: strncpy(line, strCM, line_allocation); line_length = strlen(line); break;
            case 91: strncpy(line, strCN, line_allocation); line_length = strlen(line); break;
            case 92: strncpy(line, strCO, line_allocation); line_length = strlen(line); break;
            case 93: strncpy(line, strCP, line_allocation); line_length = strlen(line); break;
            case 94: strncpy(line, strCQ, line_allocation); line_length = strlen(line); break;
            case 95: strncpy(line, strCR, line_allocation); line_length = strlen(line); break;
            case 96: strncpy(line, strCS, line_allocation); line_length = strlen(line); break;
            case 97: strncpy(line, strCT, line_allocation); line_length = strlen(line); break;
            case 98: strncpy(line, strCU, line_allocation); line_length = strlen(line); break;
            case 99: strncpy(line, strCV, line_allocation); line_length = strlen(line); break;
            */
            default: break;
        };

        // 空行をスキップ
        if(! line_length)
            continue;

        // 最初のx値
        char * token = strtok_r(line, DELIMITER, &save_state);
        mpz_set_str(x, token, 16);
        size_t token_idx = 1;

        // 0行目は特殊
        if(line_idx == 0) {
            MPZNode*head = malloc(sizeof(MPZNode));
            MPZNode*tail = head;

            // headの初期化
            head->next = NULL;
            mpz_init(head->x);
            mpz_init(head->y);

            while((token = strtok_r(save_state, DELIMITER, &save_state))) {
                if(token_idx%2 == 0) {
                    // 偶数の時はx
                    mpz_set_str(x, token, 16);
                } else {
                    // 奇数の時はy
                    mpz_set_str(y, token, 16);

                    tail->next = malloc(sizeof(MPZNode));
                    tail = tail->next;
                    tail->next = NULL;
                    mpz_init(tail->x);
                    mpz_init(tail->y);
                    mpz_set(tail->x, x);
                    mpz_set(tail->y, y);
                }
                token_idx++;
            }

            // chunksはtokenの半分に等しい
            chunks->num_chunks = token_idx/2;

            // chunks配列にメモリを割り当てる
            chunks->chunks = malloc(chunks->num_chunks*sizeof(Shares));

            // chunksごとに共有のメモリを割り当て
            size_t chunk_idx = 0;
            for(; chunk_idx<chunks->num_chunks; chunk_idx++) {
                Shares * shares = chunks->chunks+chunk_idx;
                shares->x_vals = malloc(k * sizeof(mpz_t));
                shares->y_vals = malloc(k * sizeof(mpz_t));
                shares->num_shares = k;

                // 共有内の全ての整数を初期化
                for (size_t i=0; i<k; i++) {
                    mpz_init(shares->x_vals[i]);
                    mpz_init(shares->y_vals[i]);
                }
            }

            MPZNode*tmp = head->next;
            chunk_idx = 0;

            while(tmp) {
                Shares*shares = chunks->chunks+chunk_idx;
                mpz_set(shares->x_vals[0], tmp->x);
                mpz_set(shares->y_vals[0], tmp->y);
                tmp = tmp->next;
                chunk_idx++;
            }

            // 全ての要素を解放
            while(head) {
                tmp = head->next;
                mpz_clear(head->x);
                mpz_clear(head->y);
                free(head);
                head = tmp;
            }

            // 奇数では終われない
            if(token_idx%2 == 1)
                goto cleanup_failure;
        } else {
            while ((token = strtok_r(save_state, DELIMITER, &save_state))) {
                if (token_idx/2 >= chunks->num_chunks)
                    goto cleanup_failure;

                if (token_idx%2 == 0) {
                    mpz_set_str(x, token, 16);
                } else {
                    mpz_set_str(y, token, 16);
                    size_t chunk_idx = token_idx/2;
                    Shares * shares = chunks->chunks+chunk_idx;
                    mpz_set(shares->x_vals[line_idx], x);
                    mpz_set(shares->y_vals[line_idx], y);
                }
                token_idx++;
            }
            if (token_idx%2 == 1 || token_idx/2 < chunks->num_chunks)
                goto cleanup_failure;
        }
        line_idx++;
    }

    if (line_idx < k)
        goto cleanup_failure;

    free(line);
    mpz_clear(x);
    mpz_clear(y);
    return 1;

    cleanup_failure:
        free_chunks(chunks);
        free(line);
        mpz_clear(x);
        mpz_clear(y);

    return 0;
}

//void pub(const char *strA, const char *strB, const char *strC) {
void pub(const char *strA, const char *strB, const char *strC, const char *strD, const char *strE) {
/*
void pub(const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
         const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ) {
*/
/*
void pub(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT) {
*/
/*
void pub(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD) {
*/
/*
void pub(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN) {
*/
/*
void pub(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX) {
*/
/*
void pub(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  const char *strAY, const char *strAZ, const char *strBA, const char *strBB, const char *strBC,
  const char *strBD, const char *strBE, const char *strBF, const char *strBG, const char *strBH) {
*/
/*
void pub(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  const char *strAY, const char *strAZ, const char *strBA, const char *strBB, const char *strBC,
  const char *strBD, const char *strBE, const char *strBF, const char *strBG, const char *strBH,
  const char *strBI, const char *strBJ, const char *strBK, const char *strBL, const char *strBM,
  const char *strBN, const char *strBO, const char *strBP, const char *strBQ, const char *strBR) {
*/
/*
void pub(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  const char *strAY, const char *strAZ, const char *strBA, const char *strBB, const char *strBC,
  const char *strBD, const char *strBE, const char *strBF, const char *strBG, const char *strBH,
  const char *strBI, const char *strBJ, const char *strBK, const char *strBL, const char *strBM,
  const char *strBN, const char *strBO, const char *strBP, const char *strBQ, const char *strBR,
  const char *strBS, const char *strBT, const char *strBU, const char *strBV, const char *strBW,
  const char *strBX, const char *strBY, const char *strBZ, const char *strCA, const char *strCB) {
*/
/*
void pub(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  const char *strAY, const char *strAZ, const char *strBA, const char *strBB, const char *strBC,
  const char *strBD, const char *strBE, const char *strBF, const char *strBG, const char *strBH,
  const char *strBI, const char *strBJ, const char *strBK, const char *strBL, const char *strBM,
  const char *strBN, const char *strBO, const char *strBP, const char *strBQ, const char *strBR,
  const char *strBS, const char *strBT, const char *strBU, const char *strBV, const char *strBW,
  const char *strBX, const char *strBY, const char *strBZ, const char *strCA, const char *strCB,
  const char *strCC, const char *strCD, const char *strCE, const char *strCF, const char *strCG,
  const char *strCH, const char *strCI, const char *strCJ, const char *strCK, const char *strCL) {
*/
/*
void pub(
  const char *strA, const char *strB, const char *strC, const char *strD, const char *strE,
  const char *strF, const char *strG, const char *strH, const char *strI, const char *strJ,
  const char *strK, const char *strL, const char *strM, const char *strN, const char *strO,
  const char *strP, const char *strQ, const char *strR, const char *strS, const char *strT,
  const char *strU, const char *strV, const char *strW, const char *strX, const char *strY,
  const char *strZ, const char *strAA, const char *strAB, const char *strAC, const char *strAD,
  const char *strAE, const char *strAF, const char *strAG, const char *strAH, const char *strAI,
  const char *strAJ, const char *strAK, const char *strAL, const char *strAM, const char *strAN,
  const char *strAO, const char *strAP, const char *strAQ, const char *strAR, const char *strAS,
  const char *strAT, const char *strAU, const char *strAV, const char *strAW, const char *strAX,
  const char *strAY, const char *strAZ, const char *strBA, const char *strBB, const char *strBC,
  const char *strBD, const char *strBE, const char *strBF, const char *strBG, const char *strBH,
  const char *strBI, const char *strBJ, const char *strBK, const char *strBL, const char *strBM,
  const char *strBN, const char *strBO, const char *strBP, const char *strBQ, const char *strBR,
  const char *strBS, const char *strBT, const char *strBU, const char *strBV, const char *strBW,
  const char *strBX, const char *strBY, const char *strBZ, const char *strCA, const char *strCB,
  const char *strCC, const char *strCD, const char *strCE, const char *strCF, const char *strCG,
  const char *strCH, const char *strCI, const char *strCJ, const char *strCK, const char *strCL,
  const char *strCM, const char *strCN, const char *strCO, const char *strCP, const char *strCQ,
  const char *strCR, const char *strCS, const char *strCT, const char *strCU, const char *strCV) {
*/
    struct timeval startTime, endTime;
    clock_t startClock, endClock;
    FILE *fp, *fp2;

    printf("Shamir's Secret Scheme combine start\n");

    ShareChunks chunks = {NULL, 0};
    //int k = 3;
    int k = 5;
    //int k = 10;
    //int k = 20;
    //int k = 30;
    //int k = 40;
    //int k = 50;
    //int k = 60;
    //int k = 70;
    //int k = 80;
    //int k = 90;
    //int k = 100;


    //read_chunks(strA, strB, strC, &chunks, k);
    read_chunks(strA, strB, strC, strD, strE, &chunks, k);
    //read_chunks(strA, strB, strC, strD, strE, strF, strG, strH, strI, strJ, &chunks, k);
    //read_chunks(strA, strB, strC, strD, strE, strF, strG, strH, strI, strJ, strK, strL, strM, strN, strO, strP, strQ, strR, strS, strT, &chunks, k);
/*
    read_chunks(strA, strB, strC, strD, strE, strF, strG, strH, strI, strJ,
                strK, strL, strM, strN, strO, strP, strQ, strR, strS, strT,
                strU, strV, strW, strX, strY, strZ, strAA, strAB, strAC, strAD, &chunks, k);
*/
/*
    read_chunks(strA, strB, strC, strD, strE, strF, strG, strH, strI, strJ,
                strK, strL, strM, strN, strO, strP, strQ, strR, strS, strT,
                strU, strV, strW, strX, strY, strZ, strAA, strAB, strAC, strAD,
                strAE, strAF, strAG, strAH, strAI, strAJ, strAK, strAL, strAM, strAN, &chunks, k);
*/
/*
    read_chunks(strA, strB, strC, strD, strE, strF, strG, strH, strI, strJ,
                strK, strL, strM, strN, strO, strP, strQ, strR, strS, strT,
                strU, strV, strW, strX, strY, strZ, strAA, strAB, strAC, strAD,
                strAE, strAF, strAG, strAH, strAI, strAJ, strAK, strAL, strAM, strAN,
                strAO, strAP, strAQ, strAR, strAS, strAT, strAU, strAV, strAW, strAX, &chunks, k);
*/
/*
    read_chunks(strA, strB, strC, strD, strE, strF, strG, strH, strI, strJ,
                strK, strL, strM, strN, strO, strP, strQ, strR, strS, strT,
                strU, strV, strW, strX, strY, strZ, strAA, strAB, strAC, strAD,
                strAE, strAF, strAG, strAH, strAI, strAJ, strAK, strAL, strAM, strAN,
                strAO, strAP, strAQ, strAR, strAS, strAT, strAU, strAV, strAW, strAX,
                strAY, strAZ, strBA, strBB, strBC, strBD, strBE, strBF, strBG, strBH, &chunks, k);
*/
/*
    read_chunks(strA, strB, strC, strD, strE, strF, strG, strH, strI, strJ,
                strK, strL, strM, strN, strO, strP, strQ, strR, strS, strT,
                strU, strV, strW, strX, strY, strZ, strAA, strAB, strAC, strAD,
                strAE, strAF, strAG, strAH, strAI, strAJ, strAK, strAL, strAM, strAN,
                strAO, strAP, strAQ, strAR, strAS, strAT, strAU, strAV, strAW, strAX,
                strAY, strAZ, strBA, strBB, strBC, strBD, strBE, strBF, strBG, strBH,
                strBI, strBJ, strBK, strBL, strBM, strBN, strBO, strBP, strBQ, strBR, &chunks, k);

*/
/*
    read_chunks(strA, strB, strC, strD, strE, strF, strG, strH, strI, strJ,
                strK, strL, strM, strN, strO, strP, strQ, strR, strS, strT,
                strU, strV, strW, strX, strY, strZ, strAA, strAB, strAC, strAD,
                strAE, strAF, strAG, strAH, strAI, strAJ, strAK, strAL, strAM, strAN,
                strAO, strAP, strAQ, strAR, strAS, strAT, strAU, strAV, strAW, strAX,
                strAY, strAZ, strBA, strBB, strBC, strBD, strBE, strBF, strBG, strBH,
                strBI, strBJ, strBK, strBL, strBM, strBN, strBO, strBP, strBQ, strBR,
                strBS, strBT, strBU, strBV, strBW, strBX, strBY, strBZ, strCA, strCB, &chunks, k);
*/
/*
    read_chunks(strA, strB, strC, strD, strE, strF, strG, strH, strI, strJ,
                strK, strL, strM, strN, strO, strP, strQ, strR, strS, strT,
                strU, strV, strW, strX, strY, strZ, strAA, strAB, strAC, strAD,
                strAE, strAF, strAG, strAH, strAI, strAJ, strAK, strAL, strAM, strAN,
                strAO, strAP, strAQ, strAR, strAS, strAT, strAU, strAV, strAW, strAX,
                strAY, strAZ, strBA, strBB, strBC, strBD, strBE, strBF, strBG, strBH,
                strBI, strBJ, strBK, strBL, strBM, strBN, strBO, strBP, strBQ, strBR,
                strBS, strBT, strBU, strBV, strBW, strBX, strBY, strBZ, strCA, strCB,
                strCC, strCD, strCE, strCF, strCG, strCH, strCI, strCJ, strCK, strCL, &chunks, k);
*/
/*
    read_chunks(strA, strB, strC, strD, strE, strF, strG, strH, strI, strJ,
                strK, strL, strM, strN, strO, strP, strQ, strR, strS, strT,
                strU, strV, strW, strX, strY, strZ, strAA, strAB, strAC, strAD,
                strAE, strAF, strAG, strAH, strAI, strAJ, strAK, strAL, strAM, strAN,
                strAO, strAP, strAQ, strAR, strAS, strAT, strAU, strAV, strAW, strAX,
                strAY, strAZ, strBA, strBB, strBC, strBD, strBE, strBF, strBG, strBH,
                strBI, strBJ, strBK, strBL, strBM, strBN, strBO, strBP, strBQ, strBR,
                strBS, strBT, strBU, strBV, strBW, strBX, strBY, strBZ, strCA, strCB,
                strCC, strCD, strCE, strCF, strCG, strCH, strCI, strCJ, strCK, strCL,
                strCM, strCN, strCO, strCP, strCQ, strCR, strCS, strCT, strCU, strCV, &chunks, k);
*/

    gettimeofday(&startTime, NULL); // 開始時刻取得
    startClock = clock();          // 開始時刻のCPU時間取得

    // グローバル変数の初期化
    initialize();

    size_t message_length = chunks.num_chunks*modulus_max_bytes+1;
    char message[message_length];

    // chunksを結合して元のメッセージを再構築する
    combine_chunks(message, chunks, k);
    fprintf(stdout, message);
    fprintf(stdout, "\n");

    gettimeofday(&endTime, NULL); // 開始時刻取得
    endClock = clock();           // 開始時刻のCPU時間

    time_t diffsec = difftime(endTime.tv_sec, startTime.tv_sec); // 差分処理
    suseconds_t diffsub = endTime.tv_usec-startTime.tv_usec;     // マイクロ秒部分の差分処理
    fprintf(stdout, "計測時間 : %ld.%.3ld\n", diffsec, diffsub);

    double cpusec = (double)(endClock-startClock)/(double)CLOCKS_PER_SEC;
    fprintf(stdout, "CPU時間 : %.4f\n", cpusec);

    fp = fopen("cpuData.txt", "a");
    fprintf(fp, "%.4f\n", (double)cpusec);
    fclose(fp);

    fp2 = fopen("data.txt", "a");
    fprintf(fp2, "%ld.%.3ld\n", diffsec, diffsub);
    fclose(fp2);


    free_chunks(&chunks);

    cleanup();

    exit(0);
}

//////////////////////////////////////////////////////////////////////
//                            MQTT                                  //
//////////////////////////////////////////////////////////////////////
void endCatch() {
    mosquitto_destroy(mosq);
    mosquitto_lib_cleanup();
    printf("mosquitto disconnected...\n\n");
    //pub(addA, addB, addC);
    pub(addA, addB, addC, addD, addE);
    //pub(addA, addB, addC, addD, addE, addF, addG, addH, addI, addJ);
    //pub(addA, addB, addC, addD, addE, addF, addG, addH, addI, addJ, addK, addL, addM, addN, addO, addP, addQ, addR, addS, addT);
/*
    pub(addA, addB, addC, addD, addE, addF, addG, addH, addI, addJ,
        addK, addL, addM, addN, addO, addP, addQ, addR, addS, addT,
        addU, addV, addW, addX, addY, addZ, addAA, addAB, addAC, addAD);
*/
/*
    pub(addA, addB, addC, addD, addE, addF, addG, addH, addI, addJ,
        addK, addL, addM, addN, addO, addP, addQ, addR, addS, addT,
        addU, addV, addW, addX, addY, addZ, addAA, addAB, addAC, addAD,
        addAE, addAF, addAG, addAH, addAI, addAJ, addAK, addAL, addAM, addAN);
*/
/*
    pub(addA, addB, addC, addD, addE, addF, addG, addH, addI, addJ,
        addK, addL, addM, addN, addO, addP, addQ, addR, addS, addT,
        addU, addV, addW, addX, addY, addZ, addAA, addAB, addAC, addAD,
        addAE, addAF, addAG, addAH, addAI, addAJ, addAK, addAL, addAM, addAN,
        addAO, addAP, addAQ, addAR, addAS, addAT, addAU, addAV, addAW, addAX);
*/
/*
    pub(addA, addB, addC, addD, addE, addF, addG, addH, addI, addJ,
        addK, addL, addM, addN, addO, addP, addQ, addR, addS, addT,
        addU, addV, addW, addX, addY, addZ, addAA, addAB, addAC, addAD,
        addAE, addAF, addAG, addAH, addAI, addAJ, addAK, addAL, addAM, addAN,
        addAO, addAP, addAQ, addAR, addAS, addAT, addAU, addAV, addAW, addAX,
        addAY, addAZ, addBA, addBB, addBC, addBD, addBE, addBF, addBG, addBH);
*/
/*
    pub(addA, addB, addC, addD, addE, addF, addG, addH, addI, addJ,
        addK, addL, addM, addN, addO, addP, addQ, addR, addS, addT,
        addU, addV, addW, addX, addY, addZ, addAA, addAB, addAC, addAD,
        addAE, addAF, addAG, addAH, addAI, addAJ, addAK, addAL, addAM, addAN,
        addAO, addAP, addAQ, addAR, addAS, addAT, addAU, addAV, addAW, addAX,
        addAY, addAZ, addBA, addBB, addBC, addBD, addBE, addBF, addBG, addBH,
        addBI, addBJ, addBK, addBL, addBM, addBN, addBO, addBP, addBQ, addBR);
*/
/*
    pub(addA, addB, addC, addD, addE, addF, addG, addH, addI, addJ,
        addK, addL, addM, addN, addO, addP, addQ, addR, addS, addT,
        addU, addV, addW, addX, addY, addZ, addAA, addAB, addAC, addAD,
        addAE, addAF, addAG, addAH, addAI, addAJ, addAK, addAL, addAM, addAN,
        addAO, addAP, addAQ, addAR, addAS, addAT, addAU, addAV, addAW, addAX,
        addAY, addAZ, addBA, addBB, addBC, addBD, addBE, addBF, addBG, addBH,
        addBI, addBJ, addBK, addBL, addBM, addBN, addBO, addBP, addBQ, addBR,
        addBS, addBT, addBU, addBV, addBW, addBX, addBY, addBZ, addCA, addCB);
*/
/*
    pub(addA, addB, addC, addD, addE, addF, addG, addH, addI, addJ,
        addK, addL, addM, addN, addO, addP, addQ, addR, addS, addT,
        addU, addV, addW, addX, addY, addZ, addAA, addAB, addAC, addAD,
        addAE, addAF, addAG, addAH, addAI, addAJ, addAK, addAL, addAM, addAN,
        addAO, addAP, addAQ, addAR, addAS, addAT, addAU, addAV, addAW, addAX,
        addAY, addAZ, addBA, addBB, addBC, addBD, addBE, addBF, addBG, addBH,
        addBI, addBJ, addBK, addBL, addBM, addBN, addBO, addBP, addBQ, addBR,
        addBS, addBT, addBU, addBV, addBW, addBX, addBY, addBZ, addCA, addCB,
        addCC, addCD, addCE, addCF, addCG, addCH, addCI, addCJ, addCK, addCL);
*/
/*
    pub(addA, addB, addC, addD, addE, addF, addG, addH, addI, addJ,
        addK, addL, addM, addN, addO, addP, addQ, addR, addS, addT,
        addU, addV, addW, addX, addY, addZ, addAA, addAB, addAC, addAD,
        addAE, addAF, addAG, addAH, addAI, addAJ, addAK, addAL, addAM, addAN,
        addAO, addAP, addAQ, addAR, addAS, addAT, addAU, addAV, addAW, addAX,
        addAY, addAZ, addBA, addBB, addBC, addBD, addBE, addBF, addBG, addBH,
        addBI, addBJ, addBK, addBL, addBM, addBN, addBO, addBP, addBQ, addBR,
        addBS, addBT, addBU, addBV, addBW, addBX, addBY, addBZ, addCA, addCB,
        addCC, addCD, addCE, addCF, addCG, addCH, addCI, addCJ, addCK, addCL,
        addCM, addCN, addCO, addCP, addCQ, addCR, addCS, addCT, addCU, addCV);
*/
}

void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message) {
    if(message->payloadlen) {
        printf("%s ", message->topic);
        fwrite(message->payload, 1, message->payloadlen, stdout);
        printf("\n");
        count++;
        switch(count) {
            case 1: strcat(addA, message->payload); inLenA = message->payloadlen; break;
            case 2: strcat(addB, message->payload); inLenB = message->payloadlen; break;
            case 3: strcat(addC, message->payload); inLenC = message->payloadlen; break;
            case 4: strcat(addD, message->payload); inLenD = message->payloadlen; break;
            case 5: strcat(addE, message->payload); inLenE = message->payloadlen; break;
            /*
            case 6: strcat(addF, message->payload); inLenF = message->payloadlen; break;
            case 7: strcat(addG, message->payload); inLenG = message->payloadlen; break;
            case 8: strcat(addH, message->payload); inLenH = message->payloadlen; break;
            case 9: strcat(addI, message->payload); inLenI = message->payloadlen; break;
            case 10: strcat(addJ, message->payload); inLenJ = message->payloadlen; break;

            case 11: strcat(addK, message->payload); inLenK = message->payloadlen; break;
            case 12: strcat(addL, message->payload); inLenL = message->payloadlen; break;
            case 13: strcat(addM, message->payload); inLenM = message->payloadlen; break;
            case 14: strcat(addN, message->payload); inLenN = message->payloadlen; break;
            case 15: strcat(addO, message->payload); inLenO = message->payloadlen; break;
            case 16: strcat(addP, message->payload); inLenP = message->payloadlen; break;
            case 17: strcat(addQ, message->payload); inLenQ = message->payloadlen; break;
            case 18: strcat(addR, message->payload); inLenR = message->payloadlen; break;
            case 19: strcat(addS, message->payload); inLenS = message->payloadlen; break;
            case 20: strcat(addT, message->payload); inLenT = message->payloadlen; break;

            case 21: strcat(addU, message->payload); inLenU = message->payloadlen; break;
            case 22: strcat(addV, message->payload); inLenV = message->payloadlen; break;
            case 23: strcat(addW, message->payload); inLenW = message->payloadlen; break;
            case 24: strcat(addX, message->payload); inLenX = message->payloadlen; break;
            case 25: strcat(addY, message->payload); inLenY = message->payloadlen; break;
            case 26: strcat(addZ, message->payload); inLenZ = message->payloadlen; break;
            case 27: strcat(addAA, message->payload); inLenAA = message->payloadlen; break;
            case 28: strcat(addAB, message->payload); inLenAB = message->payloadlen; break;
            case 29: strcat(addAC, message->payload); inLenAC = message->payloadlen; break;
            case 30: strcat(addAD, message->payload); inLenAD = message->payloadlen; break;

            case 31: strcat(addAE, message->payload); inLenAE = message->payloadlen; break;
            case 32: strcat(addAF, message->payload); inLenAF = message->payloadlen; break;
            case 33: strcat(addAG, message->payload); inLenAG = message->payloadlen; break;
            case 34: strcat(addAH, message->payload); inLenAH = message->payloadlen; break;
            case 35: strcat(addAI, message->payload); inLenAI = message->payloadlen; break;
            case 36: strcat(addAJ, message->payload); inLenAJ = message->payloadlen; break;
            case 37: strcat(addAK, message->payload); inLenAK = message->payloadlen; break;
            case 38: strcat(addAL, message->payload); inLenAL = message->payloadlen; break;
            case 39: strcat(addAM, message->payload); inLenAM = message->payloadlen; break;
            case 40: strcat(addAN, message->payload); inLenAN = message->payloadlen; break;

            case 41: strcat(addAO, message->payload); inLenAO = message->payloadlen; break;
            case 42: strcat(addAP, message->payload); inLenAP = message->payloadlen; break;
            case 43: strcat(addAQ, message->payload); inLenAQ = message->payloadlen; break;
            case 44: strcat(addAR, message->payload); inLenAR = message->payloadlen; break;
            case 45: strcat(addAS, message->payload); inLenAS = message->payloadlen; break;
            case 46: strcat(addAT, message->payload); inLenAT = message->payloadlen; break;
            case 47: strcat(addAU, message->payload); inLenAU = message->payloadlen; break;
            case 48: strcat(addAV, message->payload); inLenAV = message->payloadlen; break;
            case 49: strcat(addAW, message->payload); inLenAW = message->payloadlen; break;
            case 50: strcat(addAX, message->payload); inLenAX = message->payloadlen; break;

            case 51: strcat(addAY, message->payload); inLenAY = message->payloadlen; break;
            case 52: strcat(addAZ, message->payload); inLenAZ = message->payloadlen; break;
            case 53: strcat(addBA, message->payload); inLenBA = message->payloadlen; break;
            case 54: strcat(addBB, message->payload); inLenBB = message->payloadlen; break;
            case 55: strcat(addBC, message->payload); inLenBC = message->payloadlen; break;
            case 56: strcat(addBD, message->payload); inLenBD = message->payloadlen; break;
            case 57: strcat(addBE, message->payload); inLenBE = message->payloadlen; break;
            case 58: strcat(addBF, message->payload); inLenBF = message->payloadlen; break;
            case 59: strcat(addBG, message->payload); inLenBG = message->payloadlen; break;
            case 60: strcat(addBH, message->payload); inLenBH = message->payloadlen; break;

            case 61: strcat(addBI, message->payload); inLenBI = message->payloadlen; break;
            case 62: strcat(addBJ, message->payload); inLenBJ = message->payloadlen; break;
            case 63: strcat(addBK, message->payload); inLenBK = message->payloadlen; break;
            case 64: strcat(addBL, message->payload); inLenBL = message->payloadlen; break;
            case 65: strcat(addBM, message->payload); inLenBM = message->payloadlen; break;
            case 66: strcat(addBN, message->payload); inLenBN = message->payloadlen; break;
            case 67: strcat(addBO, message->payload); inLenBO = message->payloadlen; break;
            case 68: strcat(addBP, message->payload); inLenBP = message->payloadlen; break;
            case 69: strcat(addBQ, message->payload); inLenBQ = message->payloadlen; break;
            case 70: strcat(addBR, message->payload); inLenBR = message->payloadlen; break;

            case 71: strcat(addBS, message->payload); inLenBS = message->payloadlen; break;
            case 72: strcat(addBT, message->payload); inLenBT = message->payloadlen; break;
            case 73: strcat(addBU, message->payload); inLenBU = message->payloadlen; break;
            case 74: strcat(addBV, message->payload); inLenBV = message->payloadlen; break;
            case 75: strcat(addBW, message->payload); inLenBW = message->payloadlen; break;
            case 76: strcat(addBX, message->payload); inLenBX = message->payloadlen; break;
            case 77: strcat(addBY, message->payload); inLenBY = message->payloadlen; break;
            case 78: strcat(addBZ, message->payload); inLenBZ = message->payloadlen; break;
            case 79: strcat(addCA, message->payload); inLenCA = message->payloadlen; break;
            case 80: strcat(addCB, message->payload); inLenCB = message->payloadlen; break;

            case 81: strcat(addCC, message->payload); inLenCC = message->payloadlen; break;
            case 82: strcat(addCD, message->payload); inLenCD = message->payloadlen; break;
            case 83: strcat(addCE, message->payload); inLenCE = message->payloadlen; break;
            case 84: strcat(addCF, message->payload); inLenCF = message->payloadlen; break;
            case 85: strcat(addCG, message->payload); inLenCG = message->payloadlen; break;
            case 86: strcat(addCH, message->payload); inLenCH = message->payloadlen; break;
            case 87: strcat(addCI, message->payload); inLenCI = message->payloadlen; break;
            case 88: strcat(addCJ, message->payload); inLenCJ = message->payloadlen; break;
            case 89: strcat(addCK, message->payload); inLenCK = message->payloadlen; break;
            case 90: strcat(addCL, message->payload); inLenCL = message->payloadlen; break;

            case 91: strcat(addCM, message->payload); inLenCM = message->payloadlen; break;
            case 92: strcat(addCN, message->payload); inLenCN = message->payloadlen; break;
            case 93: strcat(addCO, message->payload); inLenCO = message->payloadlen; break;
            case 94: strcat(addCP, message->payload); inLenCP = message->payloadlen; break;
            case 95: strcat(addCQ, message->payload); inLenCQ = message->payloadlen; break;
            case 96: strcat(addCR, message->payload); inLenCR = message->payloadlen; break;
            case 97: strcat(addCS, message->payload); inLenCS = message->payloadlen; break;
            case 98: strcat(addCT, message->payload); inLenCT = message->payloadlen; break;
            case 99: strcat(addCU, message->payload); inLenCU = message->payloadlen; break;
            case 100: strcat(addCV, message->payload); inLenCV = message->payloadlen; break;
            */
            default : break;
        }
        if(count == 5) {
            mqtt_flag = false;
            endCatch();
        }
    } else {
        printf("%s (null)\n", message->topic);
    }

    fflush(stdout);
}

//////////////////////////////////////////////////////////////////////
//                              main                                //
//////////////////////////////////////////////////////////////////////
int main(int argc, char ** argv) {
    int split_mode = 0;
    int combine_mode = 0;
    int c;
    struct timeval startTime, endTime;
    clock_t startClock, endClock;

    while((c = getopt(argc, argv, "sc")) != -1) {
        switch(c) {
            case 's' :
                split_mode = 1;
                break;
            case 'c' :
                combine_mode = 1;
                break;
            case '?' :
                if(isprint(optopt)) {
                    fprintf(stderr, "Unknown option '-%c'.\n", optopt);
                } else {
                    fprintf(stderr, "Unknown option character '\\x%x'.\n", optopt);
                }
                return EXIT_FAILURE;
            default :
                abort();
        }
    }

    // splitもcombineも指定されていない時
    if(split_mode == combine_mode) {
        if(split_mode)
            fprintf(stderr, "Please only provide one mode-setting flag.\n");
        else
            fprintf(stderr, "No flags were provided.\nPlease use -s for split mode and -c for combine mode.\n");
        return EXIT_FAILURE;
    }

    // 引数が足りない時
    if(optind+split_mode >= argc) {
        if(split_mode)
            fprintf(stderr, "k, n need to be provided.\n");
        else
            fprintf(stderr, "k, needs to be provided.\n");
        return EXIT_FAILURE;
    }

    char * end = NULL;
    long k = strtol(argv[optind], &end, 10);

    if(*end) {
        fprintf(stderr, "Please provide an integer calue for k.\n");
        return EXIT_FAILURE;
    }

    // 引数が1以下の時
    if(k < 1) {
        fprintf(stderr, "k must be strictly greater tham 0.\n");
        return EXIT_FAILURE;
    }

    if(combine_mode) {
        const char *ip = "192.168.3.7";
        //const char *ip = "192.168.1.71"; //研究室
        //const char *ip = "192.168.1.90";
        //const char *ip = "192.168.1.91"; //raspberry pi
        const int port = 1883;
        //const int port = 8883;
        const char *username = "topsecret";
        const char *password = "password";
        const char *id = "Sub1";
        //const char *topic = "topic";
        const int sleepTime = 2;
        int ret = 0;

        signal(SIGINT, endCatch);
        printf("Prepareting mosquitto...\n");
        mosquitto_lib_init();
        mosq = mosquitto_new(id, 0, NULL);

        if(! mosq) {
            perror("Failed to create mosquitto.\n");
            mosquitto_lib_cleanup();
            return EXIT_FAILURE;
        }

        mosquitto_username_pw_set(mosq, username, password);
        //mosquitto_tls_set(mosq, "/usr/local/opt/mosquitto/etc/mosquitto/ca.crt", NULL, NULL, NULL, NULL);
        mosquitto_message_callback_set(mosq, on_message);
        mosquitto_connect(mosq, ip, port, 60);

        printf("Server (%s:%d) connected!\n", ip, port);

        mosquitto_subscribe(mosq, NULL, topic, 0);

        mosquitto_loop_forever(mosq, -1, 1);
/*
        if(mqtt_flag == true) {
            printf("wait...\n");
        } else {
            printf("Shamir's Secret Scheme combine start\n");
            ShareChunks chunks = {NULL, 0};

            if(! read_chunks(stdin, &chunks, k)) {
                fprintf(stderr, "There was a formatting error with the secret shares provided.\n");
                return EXIT_FAILURE;
            }

            // グローバル変数の初期化
            initialize();

            size_t message_length = chunks.num_chunks*modulus_max_bytes+1;
            char message[message_length];

            // chunksを結合して元のメッセージを再構築する
            combine_chunks(message, chunks, k);
            fprintf(stdout, message);
            fprintf(stdout, "\n");

            free_chunks(&chunks);

            cleanup();
        }*/
    } else {
        fprintf(stderr, "set up mode only split\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
